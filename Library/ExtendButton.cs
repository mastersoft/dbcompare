﻿using System.Windows.Controls;
using System.Windows.Media;

namespace Dasic.Library
{
    internal class ExtendButton : Button
    {
        private readonly Border _moveBgColor = new Border();
        private readonly Border _moveBorderColor = new Border();
        private readonly TextBlock _moveFontColor = new TextBlock();
        private readonly Image _moveImage = new Image();
        private readonly Image _outImage = new Image();

        /// <summary>
        ///     鼠标移进时的样式
        /// </summary>
        public ImageSource MoveImage
        {
            get { return _moveImage.Source; }
            set { _moveImage.Source = value; }
        }

        /// <summary>
        ///     鼠标移出时的样式
        /// </summary>
        public ImageSource OutImage
        {
            get { return _outImage.Source; }
            set { _outImage.Source = value; }
        }

        /// <summary>
        ///     鼠标移上时字体颜色
        /// </summary>
        public Brush MoveFontColor
        {
            get { return _moveFontColor.Foreground; }
            set { _moveFontColor.Foreground = value; }
        }

        public Brush MoveBgColor
        {
            get { return _moveBgColor.Background; }
            set { _moveBgColor.Background = value; }
        }

        public Brush MoveBorderColor
        {
            get { return _moveBorderColor.BorderBrush; }
            set { _moveBorderColor.BorderBrush = value; }
        }
    }
}